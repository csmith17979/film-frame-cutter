extends Control


func _ready():
	$Filepath.text = OS.get_system_dir(OS.SYSTEM_DIR_DESKTOP) + "/"
	get_tree().connect("files_dropped", self, "_files_dropped")


func _files_dropped(files: PoolStringArray, from_screen: int):
	$Filepath.text = files[0]


func _on_Desktop_pressed():
	$Filepath.text = OS.get_system_dir(OS.SYSTEM_DIR_DESKTOP) + "/"


func _on_Documents_pressed():
	$Filepath.text = OS.get_system_dir(OS.SYSTEM_DIR_DOCUMENTS) + "/"


func _on_Videos_pressed():
	$Filepath.text = OS.get_system_dir(OS.SYSTEM_DIR_MOVIES) + "/"


func _on_CutFrames_pressed():
	var path: String = $Filepath.text

	var prefix: String = $HBoxContainer3/Prefix.text
	var digits: int = $HBoxContainer3/FilenameDigits.value
	var frame_index: int = $HBoxContainer3/StartFrame.value

	var frames_per_image: int = $HBoxContainer2/FramesPerImages.value
	var frame_layout: int = $HBoxContainer2/FrameLayout.selected # 0 vertical, 1 horizontal

	var dir:= Directory.new()

	var files:= []

	if dir.open(path) == OK:
		dir.list_dir_begin(true)
		var file_name:= dir.get_next()
		while file_name != "":
			if dir.current_is_dir():
				print("Found directory: " + file_name)
			else:
				files.append(file_name)
			file_name = dir.get_next()
	else:
		$ErrorLabel.text = "ERROR: Can't open folder"
		$ErrorLabel.visible = true
		yield(get_tree().create_timer(5), "timeout")
		$ErrorLabel.visible = false
		return # FAILED

	if not dir.dir_exists(path.plus_file("frames")):
		dir.make_dir(path.plus_file("frames"))

	$ProgressBar.visible = true
	$CutFrames.visible = false
	for fn in files:
		# DO THE MAGIC:
		var image:= Image.new()
		if image.load(path + "/" + fn) == OK:
			var image_size = image.get_size()
			var frame_size = image_size
			if frame_layout == 0: # vertical
				frame_size.y = image_size.y / frames_per_image
			else: # horizontal
				frame_size.x = image_size.x / frames_per_image

			image.lock()
			var offset:= Vector2.ZERO
			for f in range(frames_per_image):
				var frame:= Image.new()
				frame.create(frame_size.x, frame_size.y, false, image.get_format())
				frame.lock()

				for x in range(frame_size.x):
					for y in range(frame_size.y):
						frame.set_pixel(x, y, image.get_pixel(x + offset.x, y + offset.y))

				frame.unlock()
				frame.save_png(path.plus_file("frames").plus_file(prefix + str(frame_index).pad_zeros(digits) + ".png"))

				if frame_layout == 0: # vertical
					offset.y += frame_size.y
				else: # horizontal
					offset.x += frame_size.x

				frame_index += 1

			image.unlock()

		# Update progress bar after creating frames from each input image:
		$ProgressBar.value = (float(frame_index) / float(files.size() * frames_per_image)) * 100
		yield(get_tree(), "idle_frame")

	$ProgressBar.visible = false
	$CutFrames.visible = true

	$ProgressBar.value = 0
